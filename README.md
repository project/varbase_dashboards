# Varbase Dashboards

A dashboard is what's missing for better Drupal administration experience.

This dashboard is built on top of Dashboards with Layout Builder module, utilize Drupal core's Layout Builder for dynamic dashboards management, and several enhanced blocks and widgets for an intuitive and flexible administration experience.

The idea is made to provide the site admins with an appealing and concise dashboard to become the home of any Drupal site's administration task.

Provides default set of dashboards with configuration and enhancements for dynamic default, editorial, and admin dashboard management system.

## [Varbase documentation](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/core-components/varbase-dashboards)
Check out Varbase documentation for more details.

* [Varbase Dashboards Module](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/core-components/varbase-dashboards)


Join Our Slack Team for Feedback and Support
http://slack.varbase.vardot.com/

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).